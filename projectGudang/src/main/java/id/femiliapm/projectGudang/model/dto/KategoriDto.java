package id.femiliapm.projectGudang.model.dto;

public class KategoriDto {
	private Integer codeCategory;
	private String nameCategory;

	public Integer getCodeCategory() {
		return codeCategory;
	}

	public void setCodeCategory(Integer codeCategory) {
		this.codeCategory = codeCategory;
	}

	public String getNameCategory() {
		return nameCategory;
	}

	public void setNameCategory(String nameCategory) {
		this.nameCategory = nameCategory;
	}
}
