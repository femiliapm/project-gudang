package id.femiliapm.projectGudang.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "category")
public class KategoriEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "categoryId", length = 25, nullable = false)
	private Integer kodeKategori;

	@Column(name = "categoryName")
	private String namaKategori;

	public Integer getKodeKategori() {
		return kodeKategori;
	}

	public void setKodeKategori(Integer kodeKategori) {
		this.kodeKategori = kodeKategori;
	}

	public String getNamaKategori() {
		return namaKategori;
	}

	public void setNamaKategori(String namaKategori) {
		this.namaKategori = namaKategori;
	}
}
