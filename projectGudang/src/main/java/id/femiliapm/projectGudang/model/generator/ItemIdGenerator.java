package id.femiliapm.projectGudang.model.generator;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

public class ItemIdGenerator implements IdentifierGenerator {

	@Override
	public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
		// TODO Auto-generated method stub
		String prefix = "itm";
		Connection connection = session.connection();

		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("select count(itemId) as Id from item");
			if (rs.next()) {
				int id = rs.getInt(1) + 101;
				String generateId = prefix + new Integer(id).toString();
				return generateId;
			}
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

}
