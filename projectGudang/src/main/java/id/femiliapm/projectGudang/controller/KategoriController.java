package id.femiliapm.projectGudang.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.femiliapm.projectGudang.model.dto.KategoriDto;
import id.femiliapm.projectGudang.model.entity.KategoriEntity;
import id.femiliapm.projectGudang.repository.KategoriRepository;

@RestController
@RequestMapping("/kategori")
public class KategoriController {
	private final KategoriRepository kategoriRepository;

	@Autowired
	public KategoriController(KategoriRepository kategoriRepository) {
		this.kategoriRepository = kategoriRepository;
	}

	@GetMapping
	public List<KategoriDto> get() {
		List<KategoriEntity> kategoriEntities = kategoriRepository.findAll();
		List<KategoriDto> kategoriDtos = kategoriEntities.stream().map(this::convertToDto).collect(Collectors.toList());
		return kategoriDtos;
	}

	@GetMapping("/{codeKategori}")
	public KategoriDto get(@PathVariable Integer codeKategori) {
		if (kategoriRepository.findById(codeKategori).isPresent()) {
			KategoriDto kategoriDto = convertToDto(kategoriRepository.findById(codeKategori).get());
			return kategoriDto;
		}
		return null;
	}

	@PostMapping
	public KategoriDto insert(@RequestBody KategoriDto dto) {
		KategoriEntity kategoriEntity = convertToEntity(dto);
		kategoriRepository.save(kategoriEntity);
		return convertToDto(kategoriEntity);
	}

//	@PutMapping("/{codeKategori}")
//	public

	@DeleteMapping("/{codeKategori}")
	public KategoriDto delete(@PathVariable Integer codeKategori) {
		KategoriDto kategoriDto = convertToDto(kategoriRepository.getOne(codeKategori));
		kategoriRepository.delete(convertToEntity(kategoriDto));
		return null;
	}

	private KategoriEntity convertToEntity(KategoriDto dto) {
		KategoriEntity kategoriEntity = new KategoriEntity();
		kategoriEntity.setKodeKategori(dto.getCodeCategory());
		kategoriEntity.setNamaKategori(dto.getNameCategory());
		return kategoriEntity;
	}

	private KategoriDto convertToDto(KategoriEntity kategoriEntity) {
		KategoriDto dto = new KategoriDto();
		dto.setCodeCategory(kategoriEntity.getKodeKategori());
		dto.setNameCategory(kategoriEntity.getNamaKategori());
		return dto;
	}
}
