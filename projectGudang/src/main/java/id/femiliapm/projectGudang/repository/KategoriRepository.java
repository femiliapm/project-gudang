package id.femiliapm.projectGudang.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.femiliapm.projectGudang.model.entity.KategoriEntity;

@Repository
public interface KategoriRepository extends JpaRepository<KategoriEntity, Integer>{

}
