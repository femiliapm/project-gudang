package id.femiliapm.projectGudang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectGudangApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectGudangApplication.class, args);
	}

}
